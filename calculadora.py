#!/usr/bin/python3
# _*_ coding: utf-8 _*_

def suma(x, y):
    return x + y

def resta(x, y):
    return x - y

def multiplicacion(x, y):
    return x * y

def division(x, y):
    try:
        return x / y
    except ZeroDivisionError:
        print('División entre cero.')
        return -1

if __name__ == '__main__':
    print('3 + 2 = %s' % suma(3,2))
    print('3 - 2 = %s' % resta(3,2))
    print('3 * 2 = %s' % multiplicacion(3,2))
    print('3 / 2 = %s' % division(3,2))
    print('3 / 0', division(3, 0))
